FROM continuumio/miniconda3

RUN conda create -n cinderella -c anaconda python=3.6 pyqt=5 cudnn=7.1.2 numpy==1.14.5
ENV PATH /opt/conda/envs/cinderella/bin:$PATH
CMD [ "source activate /opt/conda/envs/cinderella" ]
RUN pip install cinderella[cpu]

ADD gmodel_cinderella_20200310_N22.h5 /src/model.h5
ADD create_particles.py /src

WORKDIR /work