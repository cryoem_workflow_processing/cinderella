#!/bin/sh

FILE=gmodel_cinderella_20200310_N22.h5

if [ ! -f "$FILE" ]; then
    wget ftp://ftp.gwdg.de/pub/misc/sphire/auto2d_models/gmodel_cinderella_20200310_N22.h5
fi

docker build -t cinderella .