# Cinderella docker

Docker container for selection of cryo-em class averages with cinderella [https://sphire.mpg.de/wiki/doku.php?id=auto_2d_class_selection]

## Selection with Cinderella

Run selection with Cinderella.

```
sp_cinderella_predict.py [-h] -i INPUT -o OUTPUT -w WEIGHTS
                         [-t CONFIDENCE_THRESHOLD] [--gpu GPU]
                         [-b BATCH_SIZE] [--invertimg]
```

Options:
* `-i INPUT` - Path folder to *.mrcs file.
* `-o OUTPUT` - Path to output folder.
* `-w WEIGHTS` - Path network weights.
* `-t CONFIDENCE_THRESHOLD` - Classes with a confidence higher as that threshold are classified as good. [default: 0.5]
* `--gpu GPU` - GPU to run on. [default: -1]
* `-b BATCH_SIZE` - Number of mini-batches during prediction. [default: 32]
* `--invertimg` - inverts the images [default: False]

## Create star file for RELION

Filter RELION star file by Cinderella selection.


```
create_particles.py [-h] -o OUT -c CONFIDENCE -t THRESHOLD FILE
```

Options:
* `FILE` - RELION data star file for Cinderella input
* `-o OUT` - Output file.
* `-c CONFIDENCE` - Text file with cinderella confidence output.
* `-t THRESHOLD` - Cinderella threshold value.