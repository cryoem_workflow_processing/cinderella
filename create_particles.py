#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import argparse

def get_good_classes(confidence_file, threshold):
    good_classes = []
    with open(confidence_file, 'r') as f:
        for line in f:
            num, val = line.split()[:2]
            if float(val) > threshold:
                relion_num = str(int(num) + 1)
                good_classes.append(relion_num)
    
    return good_classes

def filter_star_file(input_file, output_file, column, values):
    with open(input_file, 'r') as f_input:
        with open(output_file, 'w') as f_output:
            running_index = 0
            for line in f_input:
                if line.startswith('loop_'):
                    running_index = 0
                elif line.startswith('_'):
                    if column in line:
                        column_num = running_index
                    running_index += 1
                elif running_index > 0 and len(line.split()) == running_index:
                    if line.split()[column_num] not in values:
                        continue
                f_output.write(line)


def main():
    parser = argparse.ArgumentParser(
        description='Create *.star files for data selected by cinderella')
    parser.add_argument('input', metavar='FILE', help='Input star file')
    parser.add_argument('-o', '--out', required=True, help="Output file")
    parser.add_argument('-c', '--confidence', required=True, help="Cinderella confidence")
    parser.add_argument('-t', '--threshold', required=True, type=float, help="Selection threshold")
    parser_args = parser.parse_args()

    input_file = parser_args.input
    out_file = parser_args.out
    confidence_file = parser_args.confidence
    threshold = parser_args.threshold

    if not os.path.isfile(input_file):
        parser.error("File {} doesn't exist".format(cxi_file))
        
    if not os.path.isfile(confidence_file):
        parser.error("File {} doesn't exist".format(cxi_file))

    if os.path.exists(out_file) and not os.path.isfile(out_file):
        parser.error("{} is a directory".format(out_file))

    os.makedirs(os.path.dirname(out_file), exist_ok=True)

    good_classes = get_good_classes(confidence_file, threshold)

    if not good_classes:
        sys.exit("Error: 0 good classes")

    filter_star_file(input_file, out_file, 'rlnClassNumber', good_classes)



if __name__ == '__main__':
    main()